#pragma once

double SqrSumm(double a, double b) 
{
	return (a * a) + (2 * a * b) + (b * b);
}